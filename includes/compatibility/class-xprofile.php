<?php
/**
 * Sync Pmpro with xProfile
 * 
 * @link       https://figarts.co
 * @since      1.1.1
 *
 * @package    Pmpfb
 * @subpackage Pmpfb/includes
 */

class Pmpfb_xProfile {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Pmpfb_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	private $data;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	private $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of the plugin.
	 */
	private $version;


	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;

    // Check if BuddyPress xProfile is enabled
    if( function_exists('bp_is_active') && bp_is_active( 'xprofile' ) == FALSE )
      return;

    $this->data = get_option('pmpfb');
    $this->pmpfb_get_pmpro_xprofile_fields();
		add_action( 'update_user_meta', array($this, 'pmpfb_sync_user_meta' ), 10, 4);
    add_action('add_user_meta', array($this, 'pmpfb_add_user_meta' ), 10, 3);

  }

  /*
    Sync xProfile fields with PMPro fields
  */
  function pmpfb_sync_user_meta($meta_id, $object_id, $meta_key, $meta_value){		
    $fields = $this->pmpfb_get_pmpro_xprofile_fields();		

    //check if this user meta is to be mirrored
    foreach($fields as $field_meta => $field_xprofile){

      if($meta_key == $field_meta){			
        //find the buddypress field
        $field = xprofile_get_field_id_from_name($field_xprofile);

        //update it
        if(!empty($field)){
          $current_data_key = array_search($field_meta, array_column($this->data['fields'], 'name'));
          if ($this->data['fields'][$current_data_key]['type'] == 'date') {
            $meta_value = date('Y-m-d H:i:s', strtotime($meta_value));
          }
          xprofile_set_field_data($field, $object_id, $meta_value);
        }
      }
    }
  }

  function pmpfb_add_user_meta($object_id, $meta_key, $meta_value){
    $this->pmpfb_sync_user_meta(NULL, $object_id, $meta_key, $meta_value);
  }


  /**
   * Gets pmpro fields with xProfile data 
   * 
   * @since      1.1.1
   * @return    array  //usermeta field => buddypress profile field
   */
  public function pmpfb_get_pmpro_xprofile_fields(){
    $fields = $this->data['fields'];
    $xprofile = array();

    if(empty($fields))
      return;
    
    foreach( $fields as $field){
      if( isset($field['xprofile_field']) && !empty($field['xprofile_field']) ){
        $xprofile[$field['name']] = $field['xprofile_field'];
      }
    }
    return $xprofile;
  }

}

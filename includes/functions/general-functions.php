<?php
/**
 * General Functions
 *
 * @package    Pmpfb
 * @subpackage Pmpfb/admin
 * @author     David Towoju (Figarts) <hello@figarts.co>
 */

/**
 * Default options for PMPForm Builder
 * 
 * @since    1.0.0
 * @param    string    Level 
 */
function pmpfb_default_options( ) {

  $defaults = array(
    'fields'    =>  pmpfb_field_locations(),
    'checkouts'   =>  '',
    'settings'    =>  array(
      'disable_email' => true,
      'disable_password' => true
    )
  );
  return apply_filters( 'pmpfb_default_options', $defaults );
}


/**
 * Field Locations
 * 
 * @since    1.0.0
 * @param    string    Level 
 */ 
function pmpfb_field_locations( ) {
  
  $field_locations = array(
    array(
      'name' => 'after_username',
      'id' => 'after_username',
      "type"=> "number",
      "label"=> esc_html__('After Username','pmpfb'),
      'className' => 'pmpfb-default-field'
    ),
    array(
      'name' => 'after_password',
      'id' => 'after_password',
      "type"=> "number",
      "label"=> esc_html__('After Password','pmpfb'),
      'className' => 'pmpfb-default-field'
    ),
    array(
      'name' => 'after_email',
      'id' => 'after_email',
      "type"=> "number",
      "label"=> esc_html__('After Email','pmpfb'),
      'className' => 'pmpfb-default-field'
    ),    
    array(
      'name' => 'after_captcha',
      'id' => 'after_captcha',
      "type"=> "number",
      "label"=> esc_html__('After Captcha','pmpfb'),
      'className' => 'pmpfb-default-field'
    ),    
    array(
      'name' => 'checkout_boxes',
      'id' => 'checkout_boxes',
      "type"=> "number",
      "label"=> esc_html__('Checkout Box: More Information','pmpfb'),
      'className' => 'pmpfb-default-field'
    ),
    array(
      'name' => 'after_billing_fields',
      'id' => 'after_billing_fields',
      "type"=> "number",
      "label"=> esc_html__('After Billing Fields','pmpfb'),
      'className' => 'pmpfb-default-field'
    ),
    array(
      'name' => 'before_submit_button',
      'id' => 'before_submit_button',
      "type"=> "number",
      "label"=> esc_html__('Before Submit Buttons','pmpfb'),
      'className' => 'pmpfb-default-field'
    ),
  );
  array_splice( $field_locations, 5, 0, pmpfb_checkout_fields() );
  return $field_locations;
}


/**
  * Converts PHP array to JSON object.
  *
  * @since    1.0.0
  * @param    array    $fields 
  */
 function pmpfb_array_to_object($fields, $checkout){
  $json = array();

  if ( ! is_array($fields)) {
    return;
  }
  $json['fields'] = $fields;
  $json['checkouts'] = $checkout;

  $json = json_encode($json);
  return $json;
}

/**
  * Converts PHP array to JSON object.
  *
  * @since    1.0.0
  * @param    array    $fields 
  */
function pmpfb_fields_to_object($fields){
  $json = array();

  if ( ! is_array($fields)) {
    return;
  }

  foreach ($fields as $field) {
    $itemObject = new stdClass();
    foreach ($field as $key => $value) {
      if (is_array($value)) {
        foreach ($value as $k => $v) {
          $itemObject->{$key}[$k] = array_map('esc_attr',$v);
        }
      }else {
        $itemObject->{$key} = esc_attr($value);
      }
    }
    array_push($json, $itemObject);
  }
  $json = json_encode($json);

  return $json;
}


/**
 * Get default and dynamically generated checkout fields 
 * 
 * @since    1.0.0
 * @param    string    Level 
 */ 
function pmpfb_checkout_fields(){
  
  $data = get_option('pmpfb');
  $checkouts = $data['checkouts'];
  
  if ( !is_array($checkouts) OR empty($checkouts) )
    return;
  
  $cb = array();
  foreach ($checkouts as $checkout) {
    $slug = str_replace(' ', '-', strtolower($checkout));
    $cb[] = array( 
      'name' => $slug,
      'id' => $slug,
      "type"=> "number",
      "label"=> $checkout,
      'className' => 'pmpfb-default-field'
    );
  }
  return $cb;
}


/**
 * Form Builder Type Attribute Settings
 *
 * @since    1.0.0
 * @param    string    $membership 
 */
function pmpfb_get_type_attr(){
    
  $type_attr = array(
    // 'typeUserAttrs' => array(
      'number' => array(
        'id' => array(
          'label' => 'ID',
        ),
      ),
      'text' => array(
        'size' => array(
          'label' => 'Size',
          'type' => 'number',
        ),
      ),
      'textarea' => array(
        'cols' => array(
          'label' => 'Cols',
          'type' => 'number',
        ),
      ),
      'select' => array( 
       'select2' => array(
          'label' => 'Select2',
          'type' => 'checkbox',
        ),
        'subtype' => array(
          'label' => 'Type',
          'options' => array(
            'custom' => 'Custom',
            'country' => 'Country',
          )
        ),              
      ),
      'checkbox' => array(  
        'text' => array(
          'label' => 'Text',
        ),            
      ),
      'radio-group' => array(       
      ),
      'date' => array(        
      ),
      'file' => array(        
        'accept' => array(
          'label' => 'Accept',
          'type' => 'text',
          'placeholder' => 'type image/* to accept only images'
        ),        
      ),
      'hidden' => array(        
      ),
      'header' => array(
       'name' => array(
          'label' => 'Name',
        ), 
      ),
      'paragraph' => array(
       'name' => array(
          'label' => 'Name',
        ), 
      )
    // )
  );

  $type_attr = apply_filters( 'pmpfb_type_attributes', $type_attr );

  $type_attr['text'] = array_merge($type_attr['text'],pmpfb_common_attr());
  $type_attr['textarea'] = array_merge($type_attr['textarea'],pmpfb_common_attr());
  $type_attr['select'] = array_merge($type_attr['select'],pmpfb_common_attr());
  $type_attr['checkbox'] = array_merge($type_attr['checkbox'],pmpfb_common_attr());
  $type_attr['radio-group'] = array_merge($type_attr['radio-group'],pmpfb_common_attr());
  $type_attr['file'] = array_merge($type_attr['file'],pmpfb_common_attr());
  $type_attr['date'] = array_merge($type_attr['date'],pmpfb_common_attr());
  $type_attr['hidden'] = array_merge($type_attr['hidden'],pmpfb_common_attr());
  $type_attr['header'] = array_merge($type_attr['header'],pmpfb_common_attr());
  $type_attr['paragraph'] = array_merge($type_attr['paragraph'],pmpfb_common_attr());

  // $type_attr = json_encode($type_attr, JSON_PRETTY_PRINT);
  // return substr($type_attr, 1, -1);
  return $type_attr;

}


/**
 * Common field attributes
 *
 * @since    1.0.0
 */
function pmpfb_common_attr()
{

  $attrs = array(
    'memberslistcsv' => array(
      'label' => 'CSV',
      'type' => 'checkbox',
    ),
    'readonly' => array(
      'label' => 'Read Only',
      'type' => 'checkbox',
    ),                 
    'depends' => array(
      'label' => 'Conditional',
      'type' => 'checkbox',
    ),
    'dependfield' => array(
      'label' => 'Field Name',
    ),
    'dependvalue' => array(
      'label' => 'Field Value',
    ),
    'profile' => array(
      'label' => 'Profile',
      'options' => array(
        'true' => 'True',
        'false' => 'False',
        'only' => 'Only',
        'only_admin' => 'Only Admin',
      )
    ),
    'showmainlabel' => array(
      'label' => 'Remove Label',
      'type' => 'checkbox',
    ),
    'divclass' => array(
      'label' => 'Div Class',
    ),
    'xprofile_field' => array(
      'label' => 'xProfile Field Name',
    ),

  );
  return $attrs;
}


/**
 * Get arrays bewtween two values
 * 
 * @since    1.0.0
 * @param    string    start 
 * @param    string    end 
 * @param    array    array 
 */
function pmpfb_getArraysBetweenNames($name1, $name2, $array)
{
  $return = [];
  $foundName1 = false;

  if ($name1 == 'jolly' || $name1 == 'fresh') {
    $name1 = ucwords($name1);
  }

  if ($name2 == 'jolly' || $name2 == 'fresh') {
    $name2 = ucwords($name2);
  }

  if (is_array($array) && !empty($array)) {
    foreach ($array as $key => $item) {
      if ($foundName1) {
        if ( isset($item['id']) && $item["id"] == $name2)
          break;

        $return[$key] = $item;
      } elseif ( isset($item['id']) && $item["id"] == $name1) {
        $foundName1 = true;
      }
    }
  }

  return $return;
}


/**
 * Format array to PMPro select field
 * 
 * @since    1.0.0
 * @param    array    array 
 */
function pmpfb_pmpro_select_format($field, $args=''){
  $options = array();
  $values = $field['values'];
  foreach ($values as $value) {
    $options[$value['value']] = $value['label'];
    if (isset($value['selected'] ) && $value['selected'] == true){
      $args['value'] = $value['value'];
    }   
  }
  return $options;
}


/**
 * PMPro Conditional logic
 * 
 * @since    1.0.0
 * @param    string    depends 
 * @param    string    id 
 * @param    string    value 
 */
function pmpfb_pmpro_conditional($depends,$id,$value){
  if ($depends == true) {
    return array(array('id' => $id, 'value' => $value));
  }
  else{
    return array();
  }
}


/**
 * Get country fields
 *
 * @since    1.1.1
 */
function pmpfb_get_countries() {

  return array("" => esc_html__("Select Country", 'rcpfb'),"AF" => "Afghanistan","AX" => "Åland Islands","AL" => "Albania","DZ" => "Algeria","AS" => "American Samoa","AD" => "Andorra","AO" => "Angola","AI" => "Anguilla","AQ" => "Antarctica","AG" => "Antigua and Barbuda","AR" => "Argentina","AM" => "Armenia","AW" => "Aruba","AU" => "Australia","AT" => "Austria","AZ" => "Azerbaijan","BS" => "Bahamas","BH" => "Bahrain","BD" => "Bangladesh","BB" => "Barbados","BY" => "Belarus","BE" => "Belgium","BZ" => "Belize","BJ" => "Benin","BM" => "Bermuda","BT" => "Bhutan","BO" => "Bolivia","BA" => "Bosnia and Herzegovina","BW" => "Botswana","BV" => "Bouvet Island","BR" => "Brazil","BQ" => "British Antarctic Territory","IO" => "British Indian Ocean Territory","VG" => "British Virgin Islands","BN" => "Brunei","BG" => "Bulgaria","BF" => "Burkina Faso","BI" => "Burundi","KH" => "Cambodia","CM" => "Cameroon","CA" => "Canada","CT" => "Canton and Enderbury Islands","CV" => "Cape Verde","KY" => "Cayman Islands","CF" => "Central African Republic","TD" => "Chad","CL" => "Chile","CN" => "China","CX" => "Christmas Island","CC" => "Cocos [Keeling] Islands","CO" => "Colombia","KM" => "Comoros","CG" => "Congo - Brazzaville","CD" => "Congo - Kinshasa","CK" => "Cook Islands","CR" => "Costa Rica","HR" => "Croatia","CU" => "Cuba","CY" => "Cyprus","CZ" => "Czech Republic","CI" => "Côte d’Ivoire","DK" => "Denmark","DJ" => "Djibouti","DM" => "Dominica","DO" => "Dominican Republic","NQ" => "Dronning Maud Land","DD" => "East Germany","EC" => "Ecuador","EG" => "Egypt","SV" => "El Salvador","GQ" => "Equatorial Guinea","ER" => "Eritrea","EE" => "Estonia","ET" => "Ethiopia","FK" => "Falkland Islands","FO" => "Faroe Islands","FJ" => "Fiji","FI" => "Finland","FR" => "France","GF" => "French Guiana","PF" => "French Polynesia","TF" => "French Southern Territories","FQ" => "French Southern and Antarctic Territories","GA" => "Gabon","GM" => "Gambia","GE" => "Georgia","DE" => "Germany","GH" => "Ghana","GI" => "Gibraltar","GR" => "Greece","GL" => "Greenland","GD" => "Grenada","GP" => "Guadeloupe","GU" => "Guam","GT" => "Guatemala","GG" => "Guernsey","GN" => "Guinea","GW" => "Guinea-Bissau","GY" => "Guyana","HT" => "Haiti","HM" => "Heard Island and McDonald Islands","HN" => "Honduras","HK" => "Hong Kong SAR China","HU" => "Hungary","IS" => "Iceland","IN" => "India","ID" => "Indonesia","IR" => "Iran","IQ" => "Iraq","IE" => "Ireland","IM" => "Isle of Man","IL" => "Israel","IT" => "Italy","JM" => "Jamaica","JP" => "Japan","JE" => "Jersey","JT" => "Johnston Island","JO" => "Jordan","KZ" => "Kazakhstan","KE" => "Kenya","KI" => "Kiribati","KW" => "Kuwait","KG" => "Kyrgyzstan","LA" => "Laos","LV" => "Latvia","LB" => "Lebanon","LS" => "Lesotho","LR" => "Liberia","LY" => "Libya","LI" => "Liechtenstein","LT" => "Lithuania","LU" => "Luxembourg","MO" => "Macau SAR China","MK" => "Macedonia","MG" => "Madagascar","MW" => "Malawi","MY" => "Malaysia","MV" => "Maldives","ML" => "Mali","MT" => "Malta","MH" => "Marshall Islands","MQ" => "Martinique","MR" => "Mauritania","MU" => "Mauritius","YT" => "Mayotte","FX" => "Metropolitan France","MX" => "Mexico","FM" => "Micronesia","MI" => "Midway Islands","MD" => "Moldova","MC" => "Monaco","MN" => "Mongolia","ME" => "Montenegro","MS" => "Montserrat","MA" => "Morocco","MZ" => "Mozambique","MM" => "Myanmar [Burma]","NA" => "Namibia","NR" => "Nauru","NP" => "Nepal","NL" => "Netherlands","AN" => "Netherlands Antilles","NT" => "Neutral Zone","NC" => "New Caledonia","NZ" => "New Zealand","NI" => "Nicaragua","NE" => "Niger","NG" => "Nigeria","NU" => "Niue","NF" => "Norfolk Island","KP" => "North Korea","VD" => "North Vietnam","MP" => "Northern Mariana Islands","NO" => "Norway","OM" => "Oman","PC" => "Pacific Islands Trust Territory","PK" => "Pakistan","PW" => "Palau","PS" => "Palestinian Territories","PA" => "Panama","PZ" => "Panama Canal Zone","PG" => "Papua New Guinea","PY" => "Paraguay","YD" => "People's Democratic Republic of Yemen","PE" => "Peru","PH" => "Philippines","PN" => "Pitcairn Islands","PL" => "Poland","PT" => "Portugal","PR" => "Puerto Rico","QA" => "Qatar","RO" => "Romania","RU" => "Russia","RW" => "Rwanda","RE" => "Réunion","BL" => "Saint Barthélemy","SH" => "Saint Helena","KN" => "Saint Kitts and Nevis","LC" => "Saint Lucia","MF" => "Saint Martin","PM" => "Saint Pierre and Miquelon","VC" => "Saint Vincent and the Grenadines","WS" => "Samoa","SM" => "San Marino","SA" => "Saudi Arabia","SN" => "Senegal","RS" => "Serbia","CS" => "Serbia and Montenegro","SC" => "Seychelles","SL" => "Sierra Leone","SG" => "Singapore","SK" => "Slovakia","SI" => "Slovenia","SB" => "Solomon Islands","SO" => "Somalia","ZA" => "South Africa","GS" => "South Georgia and the South Sandwich Islands","KR" => "South Korea","ES" => "Spain","LK" => "Sri Lanka","SD" => "Sudan","SR" => "Suriname","SJ" => "Svalbard and Jan Mayen","SZ" => "Swaziland","SE" => "Sweden","CH" => "Switzerland","SY" => "Syria","ST" => "São Tomé and Príncipe","TW" => "Taiwan","TJ" => "Tajikistan","TZ" => "Tanzania","TH" => "Thailand","TL" => "Timor-Leste","TG" => "Togo","TK" => "Tokelau","TO" => "Tonga","TT" => "Trinidad and Tobago","TN" => "Tunisia","TR" => "Turkey","TM" => "Turkmenistan","TC" => "Turks and Caicos Islands","TV" => "Tuvalu","UM" => "U.S. Minor Outlying Islands","PU" => "U.S. Miscellaneous Pacific Islands","VI" => "U.S. Virgin Islands","UG" => "Uganda","UA" => "Ukraine","SU" => "Union of Soviet Socialist Republics","AE" => "United Arab Emirates","GB" => "United Kingdom","US" => "United States","ZZ" => "Unknown or Invalid Region","UY" => "Uruguay","UZ" => "Uzbekistan","VU" => "Vanuatu","VA" => "Vatican City","VE" => "Venezuela","VN" => "Vietnam","WK" => "Wake Island","WF" => "Wallis and Futuna","EH" => "Western Sahara","YE" => "Yemen","ZM" => "Zambia","ZW" => "Zimbabwe",);
}


function pmpfb_formbuilder_languages(){
  return array('ar_TN', 'de_DE', 'en_US', 'es_ES', 'fa_IR', 'fi_FI', 'fr_FR', 'hu_HU', 'it_IT', 'nb_NO', 'nl_NL', 'pl_PL', 'pt_BR', 'ro_RO', 'ru_RU', 'tr_TR', 'vi_VN', 'zh_CN', 'zh_TW');
}

<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://figarts.co
 * @since      1.0.0
 *
 * @package    Pmpfb
 * @subpackage Pmpfb/admin
 */

class Pmpfb_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->initialize();

	}

	/**
	 * Registers plugin options
	 *
	 * @since    1.0.0
	 */
	function initialize(){
		if( false == get_option( 'pmpfb' ) ) {
		  add_option( 'pmpfb', pmpfb_default_options() );
		} // end if
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		$screen = get_current_screen();
		if (stripos( $screen->id, 'pmpro_form_builder') === false)
      return;

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/pmpfb-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		$screen = get_current_screen();
		if (stripos( $screen->id, 'pmpro_form_builder') === false)
      return;

		// wp_dump(PMPFB_DIR_URL . 'admin/js/tabs.js');
		wp_enqueue_script( 'pmpfb-fb', PMPFB_DIR_URL . 'admin/js/form-builder.min.js', array( 'jquery', 'jquery-ui-sortable' ), $this->version, true );
    wp_enqueue_script( 'pmpfb-tabs', PMPFB_DIR_URL . 'admin/js/tabs.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( 'pmpfb-repeatable', PMPFB_DIR_URL . 'admin/js/jquery.repeatable.js', array( 'jquery' ), $this->version, true );
		wp_enqueue_script( $this->plugin_name, PMPFB_DIR_URL . 'admin/js/pmpfb-admin.js', array( 'jquery' ), $this->version, true );

    $data = get_option('pmpfb');
    // Localize the script to send locale to form builder
    $translation_array = array(
      'url' => PMPFB_DIR_URL . 'languages/',
      'locale' => in_array( get_locale(), pmpfb_formbuilder_languages() ) ? get_locale() : 'en_US',
      'type_attr' => pmpfb_get_type_attr(),
      'data' => pmpfb_fields_to_object($data['fields']),
      'roles' => pmpfb_get_pmprolevels(false),
    );

    wp_localize_script( 'pmpfb-fb', 'pmpfbJS', $translation_array );
	}

	/**
	 * Adds a submenu under Restrict menu
	 *
	 * @since    1.0.0
	 */
	public function add_submenu() {
	  add_submenu_page(
  		(PMPRO_VERSION > '2') ? 'pmpro-dashboard' : 'pmpro-membershiplevels',
      esc_html__( 'Form Builder', 'pmpfb' ),  // The title
      esc_html__( 'Form Builder', 'pmpfb' ),  // The text to be displayed for this menu item
      'manage_options',                       // Which type of users can see this menu item
      'pmpro_form_builder',                     // The unique ID - that is, the slug - for this menu item
      array( $this, 'render_submenu_page' )   // The name of the function to call
	  );
	}

	/**
	 * Renders the submenu created above
	 *
	 * @since    1.0.0
	 */
	function render_submenu_page() {

    $data = get_option('pmpfb');

    $fields = pmpfb_fields_to_object($data['fields']);
		// wp_dump($fields);

    $checkouts = $data['checkouts'];
    $settings = $data['settings'];
		require_once PMPFB_ADMIN_PARTIALS . '/submenu-page.php';
	}


	/**
	 * Saves form settings
	 *
	 * @since    1.0.0
	 */
	public function save_settings(){

    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['pmpfb_form_data'])) {

      if( ! current_user_can( 'manage_options' ) )
        return;

      // Do a nonce security check
      $nonce = $_REQUEST['_wpnonce'];
      if ( ! wp_verify_nonce( $nonce, 'pmpfb_save_nonce' ) ) wp_die( 'Security check' );

      $data = [];

      if( ! empty( $_FILES['pmpfb_import_file']['name']) ){
        $name = $_FILES['pmpfb_import_file']['name'];
        $extension = end( explode( '.', $name ) );

        if( $extension != 'json' ) {
          wp_die( esc_html__( 'Please upload a valid .json file', 'rcpfb' ) );
        }

        $import_file = $_FILES['pmpfb_import_file']['tmp_name'];
        if( empty( $import_file ) ) {
          wp_die( esc_html__( 'Please upload a file to import', 'rcpfb' ) );
        }
        // Retrieve the form from the file and convert the json object to an array.

        $import_data = trim( file_get_contents( $import_file ), '"');
        $import_data = stripslashes( sanitize_text_field( $import_data ) );
        $import_data = json_decode( $import_data, TRUE);
        $data['fields'] = $import_data['fields'];
        $data['checkouts'] = $import_data['checkouts'];
      }
      else{

				$posted_fields = $_POST['pmpfb_form_data'];
	      $fields = stripslashes( sanitize_text_field($posted_fields) );
	      $data['fields'] = json_decode( $fields, TRUE);

	      if ( isset($_POST['pmpfb_checkouts']) && is_array($_POST['pmpfb_checkouts']) ) {
	      	$posted_checkouts = array_column($_POST['pmpfb_checkouts'], 'name');
	      	$posted_checkouts = array_filter($posted_checkouts);

					if( empty($posted_checkouts) ){
						$data['checkouts'] = array();
					}

					$data['checkouts'] = array_map('sanitize_text_field', $posted_checkouts);

					$existing_checkouts = get_option('pmpfb')['checkouts'];

					if ( count($existing_checkouts) > count($data['checkouts']) ) {
						// remove checkouts
						$existing_checkouts = get_option('pmpfb')['checkouts'];
						if ( is_array($data['checkouts']) && is_array($existing_checkouts) ){
							$diff = array_merge(array_diff($data['checkouts'],$existing_checkouts),array_diff($existing_checkouts,$data['checkouts']));
							if ( is_array($diff) && !empty($diff) ){
								$data['fields'] = $this->remove_checkout_from_fields($data['fields'], $diff);
							}
						}
					}
					else{
						// add checkouts
						$data['fields'] = $this->add_checkout_to_fields($data['fields'], $data['checkouts']);
					}
	    	}else{
	    		$existing_checkouts = get_option('pmpfb')['checkouts'];
	    		if ( !empty($existing_checkouts) && is_array($existing_checkouts)  ) {
	    			$data['fields'] = $this->remove_checkouts_from_fields($data['fields'], $existing_checkouts);
	    		}
	      	$data['checkouts'] = array();
	    	}

      }

    	// SETTINGS
      if ( isset($_POST['pmpfb_settings']) && is_array($_POST['pmpfb_settings']) ) {
      	$data['settings'] = array_map('sanitize_text_field', $_POST['pmpfb_settings']);
      }else{
        $data['settings'] = array();
      }

      update_option('pmpfb', $data);
    }


	}


	/**
	 * Adds checkouts to field positions
	 *
	 * @since    1.0.0
	 */
	function add_checkout_to_fields($fields,$checkouts){


		foreach ($checkouts as $checkout) {

			$slug = str_replace(' ', '-', strtolower($checkout));

      if(false != ($checkout_exists = array_search($slug, array_column($fields, 'name')))){
				continue;
			}

			$field = array(
				array(
				'name' => $slug,
				'id' => $slug,
		    "type"=> "number",
		    "label"=> $checkout,
		    'className' => 'pmpfb-default-field'
				)
			);

			$after_billing_fields = array_search('after_billing_fields', array_column($fields, 'name'));

			array_splice($fields, $after_billing_fields, 0, $field);
		}
		return $fields;
	}




	/**
	 * Adds checkouts to field positions
	 *
	 * @since    1.0.0
	 */
	function remove_checkout_from_fields($fields,$diff){

		$o_fields = $fields;
		foreach ($diff as $slug) {
			$key = array_search($slug, array_column($o_fields, 'name'));
			unset($fields[$key]);
		}

		return $fields;
	}


	/**
	 * Adds checkouts to field positions
	 *
	 * @since    1.0.0
	 */
	function remove_checkouts_from_fields($fields,$checkouts){

		$o_fields = $fields;
		foreach ($checkouts as $checkout) {
			$key = array_search($checkout, array_column($o_fields, 'name'));
			unset($fields[$key]);
		}
		return $fields;
	}


	/**
	 * Process Form Export
	 *
	 * @since    1.0.0
	 */
  function process_form_export() {

    if( empty( $_GET['pmpfb_export'] ) || '1' != $_GET['pmpfb_export'] )
      return;

    if (!isset($_GET['pmpfb_nonce']) || !wp_verify_nonce($_GET['pmpfb_nonce'], 'exporting_pmpfb'))
      return;

    if( ! current_user_can( 'manage_options' ) )
      return;


    $data = get_option('pmpfb');
    $fields = pmpfb_array_to_object($data['fields'], $data['checkouts']);
    ignore_user_abort( true );

    nocache_headers();
    header( 'Content-Type: application/json; charset=utf-8' );
    header( 'Content-Disposition: attachment; filename=pmpro-fields-' . date( 'm-d-Y' ) . '.json' );
    header( "Expires: 0" );

    echo json_encode( $fields );
    exit;
  }

	/**
	 * Reset Form
	 *
	 * @since    1.0.0
	 */
  function reset_form() {

    if( empty( $_GET['pmpfb_reset'] ) || '1' != $_GET['pmpfb_reset'] )
      return;

    if (!isset($_GET['pmpfb_nonce']) || !wp_verify_nonce($_GET['pmpfb_nonce'], 'resets_pmpfb'))
      return;

    if( ! current_user_can( 'manage_options' ) )
      return;

		delete_option('pmpfb');
		wp_safe_redirect( admin_url('admin.php?page=pmpro_form_builder') ); exit;
  }
}
